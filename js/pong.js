var raquette_gauche = 325;
var raquette_droite = 325;
var raquette_gauche_deplacement = 0;
var raquette_droite_deplacement = 0;
var balle_x = 390;
var balle_y = 340;
var balle_x_deplacement = 1.3;
var balle_y_deplacement = 1.3;
var compteur_g = 0;
var compteur_d = 0;
var start_btn = document.getElementById('start_btn');
var nombre_points;
var victoire = false;
var timer = 0;

const largeur_ecran = 800;
const hauteur_ecran = 700;
const largeur_balle = 20;
const hauteur_balle = 20;
const largeur_raquette = 20;
const hauteur_raquette = 100;
const hauteur_max = hauteur_ecran - hauteur_raquette;
const bord_gauche = largeur_raquette;
const bord_droit = largeur_ecran - largeur_raquette - largeur_balle;
const bord_haut = 10;
const bord_bas = hauteur_ecran - hauteur_balle;


// Cette fonction permet de récupérer le nombre de tours voulus par les joueurs
// Puis de lancer l'interval qui gère la partie
function nombre_points(){
  document.getElementById('choix_nb_tours').style.visibility='hidden';
  nombre_points = document.getElementById("nombre_points").value;

  timer = setInterval(function(){
    bouge_raquettes();
    bouge_balle();
    compteurs();
    conditions_victoire();
    victoire = false;
  },5);
}

// Cette fonction permet de faire bouger les bouge_raquettes
// Tout en leur imposant une limite de déplacement
// A savoir les bordures du haut et du bas
function bouge_raquettes(){
  raquette_gauche += raquette_gauche_deplacement;
  if (raquette_gauche<0) {
    raquette_gauche=0;
  }
  if (raquette_gauche>hauteur_max){
    raquette_gauche=hauteur_max;
  }
  raquette_droite += raquette_droite_deplacement;
  if (raquette_droite<0) {
    raquette_droite=0;
  }
  if (raquette_droite>hauteur_max){
    raquette_droite=hauteur_max;
  };
  document.getElementById("raquette_gauche").style.top=raquette_gauche+"px";
  document.getElementById("raquette_droite").style.top=raquette_droite+"px";
}

// Cette fonction permet de mettre à joueurs les compteurs en front
function compteurs(){
  document.getElementById('compteur_g').innerHTML=compteur_g;
  document.getElementById('compteur_d').innerHTML=compteur_d;
}

// Cette fonction gère le déplacement de la balle sur les diagonales
// Ses rebonds au contact des raquettes
// Les points et les affichages si elle touche un des bords
function bouge_balle(){

  balle_x += balle_x_deplacement;
  balle_y += balle_y_deplacement;

  if (balle_y<0-10) {
    balle_y_deplacement = -balle_y_deplacement;
  };
  if (balle_y>hauteur_ecran-10){
    balle_y_deplacement = -balle_y_deplacement;
  };

  if (balle_x<bord_gauche-10) {
    balle_x_deplacement = -balle_x_deplacement;
  };
  if (balle_x>bord_droit+10){
    balle_x_deplacement = -balle_x_deplacement;
  };

  if (balle_x < bord_gauche) {
    if ((balle_y < raquette_gauche - hauteur_balle)||(balle_y > raquette_gauche + hauteur_raquette)){
      clearInterval(timer);
      document.getElementById('message_partie').innerHTML='<p>+1 point pour le joueur de droite !</p>';
      document.getElementById('message_partie').style.visibility='visible';
      document.getElementById('restart_btn').innerHTML='Continuer !';
      document.getElementById('restart_btn').style.visibility='visible';
      compteur_d +=1;
    }
  }

  if (balle_x > bord_droit) {
    if ((balle_y < raquette_droite - hauteur_balle)||(balle_y > raquette_droite + hauteur_raquette)){
      clearInterval(timer);
      document.getElementById('message_partie').innerHTML='<p>+1 point pour le joueur de gauche !</p>';
      document.getElementById('message_partie').style.visibility='visible';
      document.getElementById('restart_btn').innerHTML='Continuer !';
      document.getElementById('restart_btn').style.visibility='visible';
      compteur_g +=1;
    }
  }
  document.getElementById("balle").style.left=balle_x+"px";
  document.getElementById("balle").style.top=balle_y+"px";

}

// Cette fonctrion permet de vérifier si l'un des joueurs à gagné la partie
// et tous les affichaghes associés
function conditions_victoire(){
  if (compteur_d == nombre_points) {
    document.getElementById('message_partie').style.visibility='hidden';
    document.getElementById('message_victoire').innerHTML='<p>Victoire pour le joueur de droite !</p>';
    document.getElementById('message_victoire').style.visibility='visible';
    document.getElementById('restart_btn').innerHTML='Rejouer !';
    document.getElementById("restart_btn").addEventListener("click", reload);
    document.getElementById("ecran").className = "ecran_color";
    victoire = true;
  };
  if (compteur_g == nombre_points) {
    document.getElementById('message_partie').style.visibility='hidden';
    document.getElementById('message_victoire').innerHTML='<p>Victoire pour le joueur de gauche !</p>';
    document.getElementById('message_victoire').style.visibility='visible';
    document.getElementById('restart_btn').innerHTML='Rejouer !';
    document.getElementById("restart_btn").addEventListener("click", reload);
    document.getElementById("ecran").className = "ecran_color";
    victoire = true;
  };
}


// Fonction réinitialisant les variables nécéssaires au tour suivant
// On y fait appel au click entre deux tours
// Elle réinitialise les variables sans avoir à refresh
// Ce qui permet de garder les valeurs dans les compteurs
function restart_btn(){
  raquette_gauche = 325;
  raquette_droite = 325;
  balle_x = 390;
  balle_y = 340;

  document.getElementById('message_partie').style.visibility='hidden';
  document.getElementById('restart_btn').style.visibility='hidden';
  document.getElementById('compteur_g').innerHTML=compteur_g;
  document.getElementById('compteur_d').innerHTML=compteur_d;
  document.getElementById("raquette_gauche").style.top=raquette_gauche+"px";
  document.getElementById("raquette_droite").style.top=raquette_droite+"px";
  document.getElementById("balle").style.left=balle_x+"px";
  document.getElementById("balle").style.top=balle_y+"px";
  document.getElementById("ecran").className = "ecran_black";

  timer = setInterval(function(){
    bouge_raquettes();
    bouge_balle();
    compteurs();
    conditions_victoire();
    victoire = false;
  },5);
}



// permet de reload la page entre deux parties
function reload(){
  document.location.reload(true);
}



// Fonction permettant de gérer l'appuis sur les touches
function keydown(e) {
  e.stopPropagation();
  e.preventDefault();
  if (e.key=='a') {
    raquette_gauche_deplacement=-2;
  }
  if (e.key=='q') {
    raquette_gauche_deplacement=2;
  }
  if (e.key=='p') {
    raquette_droite_deplacement=-2;
  }
  if (e.key=='m') {
    raquette_droite_deplacement=2;
  }
}



// Fonction permettant de gérer le relachement des touches
function keyup(e) {
  e.stopPropagation();
  e.preventDefault();
  if (e.key=='a') {
    raquette_gauche_deplacement=0;
  }
  if (e.key=='q') {
    raquette_gauche_deplacement=0;
  }
  if (e.key=='p') {
    raquette_droite_deplacement=0;
  }
  if (e.key=='m') {
    raquette_droite_deplacement=0;
  }
}



// Fonction permettant de lancer une musique au click
// Google Chrome ne permettant plus l'utilisation de l'autoplay
function playSound() {
  var sound = document.getElementById("audio");
  sound.play();
}

function btn_jouer(){
  document.getElementById('introduction').style.visibility='hidden';
  document.getElementById('choix_nb_tours').style.visibility='visible';
}

window.addEventListener("keydown", keydown,true);
window.addEventListener("keyup", keyup,true);
document.getElementById("btn_jouer").addEventListener("click", btn_jouer);
document.getElementById("restart_btn").addEventListener("click", restart_btn);
document.getElementById("submit").addEventListener("click", nombre_points);
document.getElementById("btn_jouer").addEventListener("click", playSound);
